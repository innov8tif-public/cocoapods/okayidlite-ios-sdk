# OkayIDLite iOS

![Version](https://img.shields.io/cocoapods/v/OkayIDLite.svg?style=flat)
![Platform](https://img.shields.io/cocoapods/p/OkayIDLite.svg?style=flat)

Requirements:

- iOS 11+ (arm64)
- Swift 5+

## Contents

- [Installation](#installation)
- [Usage](#usage)

## Installation

Add the following to your app target.

```rb
  use_frameworks!
  pod 'OkayIDLite'

  post_install do |installer|
    installer.pods_project.targets.each do |target|
      if target.name == "CryptoSwift"
        puts "Enable module stability for CryptoSwift"
        target.build_configurations.each do |config|
            config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
        end
      end
    end
  end
```

Run `pod install`.

## Usage

Start by adding `NSCameraUsageDescription` to your `info.plist`.

Import `OkayIDLite` module into your swift file.

```swift
import OkayIDLite
```

1- Build the configuration object.

```swift
let config = Config.Builder()
    .setExtractAddress(true) // Toggle address extraction
    .setExtractGender(true) // Toggle gender extraction
    .setExtractReligion(true) // Toggle religion extraction
    .build()
```

2- Start the process by calling `startMyKadScanning` on `OkayIDLite` class with the config object. You must also pass a valid license string and a reference to your view controller. In the below example, `self` is the refers to the current view controller.

You can handle the result in the completion handler.

```swift
OkayIDLiteSDK.startMyKadScanning(
    viewController: self,
    license: lic,
    config: config
)
{ success, errorCode, result in
    if(success) {
        // handle success
    } else {
        // handle error
    }
}
```

If unsuccessful, the result will be nil.

## `MyKadResult`

| Property Name    | Description                                                          |
| ---------------- | -------------------------------------------------------------------- |
| fullName         | Full name                                                            |
| icNumber         | IC number                                                            |
| gender           | Gender                                                               |
| dob              | Date of birth                                                        |
| religion         | Religion                                                             |
| address          | Full address                                                         |
| fullDocumentPath | Path to the full document image captured during the scanning process |

This is the result type. All values are `String`s, except for `fullDocumentPath` which is a `URL`.

## Error Codes

`errorCode` is an `Int` which can have the following values:

| Error Code | Constant Name                               | Description                                                              |
| ---------- | ------------------------------------------- | ------------------------------------------------------------------------ |
| -1         | MyKadResult.NO_ERROR                        | MyKad has been successfully scanned                                      |
| 1          | MyKadResult.ERROR_INVALID_LICENSE_KEY       | The license is invalid                                                   |
| 2          | MyKadResult.ERROR_PERMISSION_DENIED         | The use did not grant the permission to access camera in order to do OCR |
| 3          | MyKadResult.ERROR_USER_CANCEL_ACTION        | The user canceled during the scanning process                            |
| 4          | MyKadResult.ERROR_UNSUPPORTED_CAMERA_PRESET | The device's camera does not support the required preset for video OCR.  |
